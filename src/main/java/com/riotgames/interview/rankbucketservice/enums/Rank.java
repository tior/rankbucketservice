package com.riotgames.interview.rankbucketservice.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Rank {
  BRONZE("bronze"),
  SILVER("silver"),
  GOLD("gold");

  @JsonValue
  public String getRank() {
    return rank;
  }

  private final String rank;

  Rank(String rank) {
    this.rank = rank.toLowerCase();
  }
}
