package com.riotgames.interview.rankbucketservice.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum GameType {
  _1V1("1v1"),
  _3V3("3v3"),
  _5V5("5v5");

  private final String gameType;

  GameType(String gameType) {
    this.gameType = gameType.toLowerCase();
  }

  @JsonValue
  public String getGameType() {
    return gameType;
  }
  ;
}
