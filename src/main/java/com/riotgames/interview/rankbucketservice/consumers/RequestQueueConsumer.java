package com.riotgames.interview.rankbucketservice.consumers;

import com.riotgames.interview.rankbucketservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.rankbucketservice.producers.GameTypeQueueProducer;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RequestQueueConsumer {
  private final ReactiveKafkaConsumerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaConsumerTemplate;
  private final GameTypeQueueProducer gameTypeQueueProducer;

  public RequestQueueConsumer(
      @Autowired
          ReactiveKafkaConsumerTemplate<String, QueueRequestMessageDto>
              reactiveKafkaConsumerTemplate,
      @Autowired GameTypeQueueProducer gameTypeQueueProducer) {
    this.reactiveKafkaConsumerTemplate = reactiveKafkaConsumerTemplate;
    this.gameTypeQueueProducer = gameTypeQueueProducer;
  }

  @PostConstruct
  public void consume() {
    reactiveKafkaConsumerTemplate
        .receive()
        .flatMap(
            stringQueueRequestMessageDtoReceiverRecord -> {
              log.info("Received {}", stringQueueRequestMessageDtoReceiverRecord);
              return gameTypeQueueProducer
                  .send(stringQueueRequestMessageDtoReceiverRecord.value())
                  .then(stringQueueRequestMessageDtoReceiverRecord.receiverOffset().commit());
            })
        .subscribe();
  }

  @PreDestroy
  public void closeConsumer() {
    reactiveKafkaConsumerTemplate
        .assignment()
        .delayElements(Duration.ofSeconds(20))
        .flatMap(reactiveKafkaConsumerTemplate::pause)
        .subscribe();
  }
}
