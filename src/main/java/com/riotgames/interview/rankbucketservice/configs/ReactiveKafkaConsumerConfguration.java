package com.riotgames.interview.rankbucketservice.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.riotgames.interview.rankbucketservice.dtos.QueueRequestMessageDto;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import reactor.kafka.receiver.ReceiverOptions;

@Configuration
public class ReactiveKafkaConsumerConfguration {
  @Bean
  public ReceiverOptions<String, QueueRequestMessageDto> kafkaReceiverOptions(
      ObjectMapper objectMapper,
      @Value(value = "${REQUEST_QUEUE_TOPIC}") String topic,
      KafkaProperties kafkaProperties) {
    ReceiverOptions<String, QueueRequestMessageDto> basicReceiverOptions =
        ReceiverOptions.create(kafkaProperties.buildConsumerProperties());
    JsonDeserializer<QueueRequestMessageDto> jsonDeserializer =
        new JsonDeserializer<>(QueueRequestMessageDto.class);
    jsonDeserializer.trustedPackages("*");
    jsonDeserializer.ignoreTypeHeaders();
    return basicReceiverOptions
        .subscription(Collections.singletonList(topic))
        .withValueDeserializer(jsonDeserializer);
  }

  @Bean
  public ReactiveKafkaConsumerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaConsumerTemplate(
          ReceiverOptions<String, QueueRequestMessageDto> kafkaReceiverOptions) {
    return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
  }
}
