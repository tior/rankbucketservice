package com.riotgames.interview.rankbucketservice.dtos;

import com.riotgames.interview.rankbucketservice.enums.GameType;
import com.riotgames.interview.rankbucketservice.enums.Rank;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Builder
@Getter
@Jacksonized
@ToString
public class QueueRequestMessageDto {
  private String userId;
  private String queueId;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
}
