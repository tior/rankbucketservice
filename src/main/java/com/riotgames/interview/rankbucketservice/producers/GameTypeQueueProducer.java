package com.riotgames.interview.rankbucketservice.producers;

import com.riotgames.interview.rankbucketservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.rankbucketservice.enums.GameType;
import com.riotgames.interview.rankbucketservice.enums.Rank;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class GameTypeQueueProducer {

  // Bronze
  @Value("${BRONZE_PLAYERS_1V1_TOPIC}")
  private String BRONZE_PLAYERS_1V1_TOPIC;

  @Value("${BRONZE_PLAYERS_3V3_TOPIC}")
  private String BRONZE_PLAYERS_3V3_TOPIC;

  @Value("${BRONZE_PLAYERS_5V5_TOPIC}")
  private String BRONZE_PLAYERS_5V5_TOPIC;

  // Silver
  @Value("${SILVER_PLAYERS_1V1_TOPIC}")
  private String SILVER_PLAYERS_1V1_TOPIC;

  @Value("${SILVER_PLAYERS_3V3_TOPIC}")
  private String SILVER_PLAYERS_3V3_TOPIC;

  @Value("${SILVER_PLAYERS_5V5_TOPIC}")
  private String SILVER_PLAYERS_5V5_TOPIC;

  // Gold
  @Value("${GOLD_PLAYERS_1V1_TOPIC}")
  private String GOLD_PLAYERS_1V1_TOPIC;

  @Value("${GOLD_PLAYERS_3V3_TOPIC}")
  private String GOLD_PLAYERS_3V3_TOPIC;

  @Value("${GOLD_PLAYERS_5V5_TOPIC}")
  private String GOLD_PLAYERS_5V5_TOPIC;

  private Map<Rank, Map<GameType, String>> rankGameTypeToTopicMapping;

  private final ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaProducerTemplate;

  public GameTypeQueueProducer(
      @Autowired
          ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
              reactiveKafkaProducerTemplate) {
    this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
  }

  public Mono<Void> send(QueueRequestMessageDto queueRequestMessageDto) {
    return reactiveKafkaProducerTemplate
        .send(
            determineTopic(queueRequestMessageDto.getRank(), queueRequestMessageDto.getGameType()),
            queueRequestMessageDto.getUserId(),
            queueRequestMessageDto)
        .then();
  }

  @PostConstruct
  public void populateRankGameTypeToTopicMapping() {
    this.rankGameTypeToTopicMapping = new HashMap<>();
    rankGameTypeToTopicMapping.put(Rank.BRONZE, new HashMap<>());
    rankGameTypeToTopicMapping.get(Rank.BRONZE).put(GameType._1V1, BRONZE_PLAYERS_1V1_TOPIC);
    rankGameTypeToTopicMapping.get(Rank.BRONZE).put(GameType._3V3, BRONZE_PLAYERS_3V3_TOPIC);
    rankGameTypeToTopicMapping.get(Rank.BRONZE).put(GameType._5V5, BRONZE_PLAYERS_5V5_TOPIC);

    rankGameTypeToTopicMapping.put(Rank.SILVER, new HashMap<>());
    rankGameTypeToTopicMapping.get(Rank.SILVER).put(GameType._1V1, SILVER_PLAYERS_1V1_TOPIC);
    rankGameTypeToTopicMapping.get(Rank.SILVER).put(GameType._3V3, SILVER_PLAYERS_3V3_TOPIC);
    rankGameTypeToTopicMapping.get(Rank.SILVER).put(GameType._5V5, SILVER_PLAYERS_5V5_TOPIC);

    rankGameTypeToTopicMapping.put(Rank.GOLD, new HashMap<>());
    rankGameTypeToTopicMapping.get(Rank.GOLD).put(GameType._1V1, GOLD_PLAYERS_1V1_TOPIC);
    rankGameTypeToTopicMapping.get(Rank.GOLD).put(GameType._3V3, GOLD_PLAYERS_3V3_TOPIC);
    rankGameTypeToTopicMapping.get(Rank.GOLD).put(GameType._5V5, GOLD_PLAYERS_5V5_TOPIC);
  }

  private String determineTopic(Rank rank, GameType gameType) {
    return this.rankGameTypeToTopicMapping.get(rank).get(gameType);
  }

  @PreDestroy
  public void closeSender() {
    reactiveKafkaProducerTemplate.close();
  }
}
