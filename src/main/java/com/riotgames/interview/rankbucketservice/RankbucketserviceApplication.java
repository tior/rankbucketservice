package com.riotgames.interview.rankbucketservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RankbucketserviceApplication {

  public static void main(String[] args) {
    SpringApplication.run(RankbucketserviceApplication.class, args);
  }
}
