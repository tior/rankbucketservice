package com.riotgames.interview.rankbucketservice.consumers;

import static org.mockito.Mockito.*;

import com.riotgames.interview.rankbucketservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.rankbucketservice.producers.GameTypeQueueProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.ReceiverOffset;
import reactor.kafka.receiver.ReceiverRecord;

class RequestQueueConsumerTest {

  private RequestQueueConsumer requestQueueConsumer;
  private ReactiveKafkaConsumerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaConsumerTemplate;
  private GameTypeQueueProducer gameTypeQueueProducer;

  @BeforeEach
  void setUp() {
    reactiveKafkaConsumerTemplate = mock(ReactiveKafkaConsumerTemplate.class);
    gameTypeQueueProducer = mock(GameTypeQueueProducer.class);
    requestQueueConsumer =
        new RequestQueueConsumer(reactiveKafkaConsumerTemplate, gameTypeQueueProducer);
  }

  @Test
  void consume() {
    ReceiverRecord<String, QueueRequestMessageDto> receiverRecord = mock(ReceiverRecord.class);
    QueueRequestMessageDto queueRequestMessageDto = QueueRequestMessageDto.builder().build();
    when(receiverRecord.value()).thenReturn(queueRequestMessageDto);

    when(gameTypeQueueProducer.send(queueRequestMessageDto)).thenReturn(Mono.empty());

    ReceiverOffset receiverOffset = mock(ReceiverOffset.class);
    when(receiverRecord.receiverOffset()).thenReturn(receiverOffset);
    when(receiverOffset.commit()).thenReturn(Mono.empty());

    when(reactiveKafkaConsumerTemplate.receive()).thenReturn(Flux.just(receiverRecord));

    requestQueueConsumer.consume();

    verify(gameTypeQueueProducer, times(1)).send(queueRequestMessageDto);
    verify(receiverOffset, times(1)).commit();
  }
}
