package com.riotgames.interview.rankbucketservice.producers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.riotgames.interview.rankbucketservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.rankbucketservice.enums.GameType;
import com.riotgames.interview.rankbucketservice.enums.Rank;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@TestPropertySource(
    properties = {
      "BRONZE_PLAYERS_1V1_TOPIC=test-topic-1v1",
    })
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = GameTypeQueueProducer.class)
class GameTypeQueueProducerTest {
  @MockBean
  private ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaProducerTemplate;

  @Autowired private GameTypeQueueProducer gameTypeQueueProducerUnderTest;

  @Test
  void send() {
    QueueRequestMessageDto queueRequestMessageDto =
        QueueRequestMessageDto.builder()
            .userId("user123")
            .rank(Rank.BRONZE)
            .gameType(GameType._1V1)
            .createdDateTime(OffsetDateTime.now())
            .build();

    when(reactiveKafkaProducerTemplate.send(anyString(), anyString(), any()))
        .thenReturn(Mono.empty());

    Mono<Void> result = gameTypeQueueProducerUnderTest.send(queueRequestMessageDto);

    StepVerifier.create(result).verifyComplete();

    verify(reactiveKafkaProducerTemplate, times(1))
        .send(eq("test-topic-1v1"), eq("user123"), eq(queueRequestMessageDto));
  }
}
