FROM openjdk:17-jdk-slim-buster
WORKDIR /app
COPY target/rankbucketservice-*-SNAPSHOT.jar myapp.jar
CMD java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8091 -jar myapp.jar

